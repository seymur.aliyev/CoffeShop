## Description

Simple shopping cart project in Laravel with several vue.js components.

## Setup 

php artisan migrate

php artisan db:seed

npm install && npm run watch
