@extends('layout.app')

@section('content')

    <div class="section">
        <div class="container">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif

            @if(session()->has('error'))
                <div class="alert alert-danger">
                    {{ session()->get('error') }}
                </div>
            @endif

            <form method="POST" action="{{ route("order") }}">
                @csrf

                <div class="row">
                    <div class="col-xl-7">

                        <div class="andro_notice-content">
                            <p>If you have a coupon code, apply it below</p>
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Coupon Code">
                                <div class="input-group-append">
                                    <button class="andro_btn-custom shadow-none" type="button">Apply Code</button>
                                </div>
                            </div>
                        </div>

                        <!-- Buyer Info Start -->
                        <h4>Billing Details</h4>
                        <div class="row">
                            <div class="form-group col-xl-6">
                                <label>First Name <span class="text-danger">*</span></label>
                                <input type="text" placeholder="First Name" name="firstname" class="form-control" required>
                            </div>
                            <div class="form-group col-xl-6">
                                <label>Last Name <span class="text-danger">*</span></label>
                                <input type="text" placeholder="Last Name" name="lastname" class="form-control" required>
                            </div>
                            <div class="form-group col-xl-6">
                                <label>Address<span class="text-danger">*</span></label>
                                <input type="text" placeholder="Baku H.Seyidzadə küç 50" name="address" class="form-control" required>
                            </div>
                            <div class="form-group col-xl-6">
                                <label>Phone Number <span class="text-danger">*</span></label>
                                <input type="text" placeholder="+994551111111" name="phone" class="form-control" required>
                            </div>
                            <div class="form-group col-xl-6">
                                <label>Email Address <span class="text-danger">*</span></label>
                                <input type="email" placeholder="Email Address" name="email" class="form-control" required>
                            </div>
                            <div class="form-group col-xl-12 mb-0">
                                <label>Order Notes</label>
                                <textarea name="note" rows="5" class="form-control" placeholder="Order Notes (Optional)"></textarea>
                            </div>
                        </div>
                        <!-- Buyer Info End -->

                    </div>
                    @if (session()->has('cart'))
                        <div class="col-xl-5 checkout-billing">
                            <!-- Order Details Start -->
                            <table class="andro_responsive-table">
                                <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Qunantity</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(session()->get('cart') as $product)
                                    <tr>
                                        <td data-title="Product">
                                            <div class="andro_cart-product-wrapper">
                                                <div class="andro_cart-product-body">
                                                    <h6> <a href="#">{{$product['title']}}</a> </h6>
                                                </div>
                                            </div>
                                        </td>
                                        <td data-title="Quantity">x{{$product['quantity']}}</td>
                                        <td data-title="Total"> <strong>{{$product['price'] * $product['quantity']}} azn</strong> </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            {{--<div class="form-group">
                                <label>Card Number</label>
                                <input type="text" class="form-control" name="master-number" placeholder="Card Number" value="">
                            </div>
                            <div class="form-group">
                                <label>Full Name</label>
                                <input type="text" class="form-control" name="master-name" placeholder="Full Name" value="">
                            </div>
                            <div class="row">
                                <div class="col-xl-6 form-group">
                                    <label>Expiry Date</label>
                                    <input type="text" class="form-control" name="master-expiry" placeholder="Expiry Date (MM/YY)" value="">
                                </div>
                                <div class="col-xl-6 form-group">
                                    <label>CVV*</label>
                                    <input type="number" class="form-control" name="master-cvv" placeholder="CVV" value="">
                                </div>
                            </div>--}}

                            <p class="small">Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our <a class="btn-link" href="#">privacy policy.</a> </p>
                            <button type="submit" class="andro_btn-custom primary btn-block">Place Order</button>
                        </div>
                    @endif
                </div>
            </form>
        </div>
    </div>
@endsection
