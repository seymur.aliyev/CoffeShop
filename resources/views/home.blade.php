@extends('layout.app')

@section('content')

<!-- Banner Start -->
<div class="andro_banner banner-2">
    <div class="andro_banner-slider">
        <div class="andro_banner-slider-inner" style="background-image: url('assets/img/banner/1.jpg');">
            <div class="container">
                <div class="andro_banner-slider-text">
                    <img src="assets/img/products/8.png" class="img-1" alt="product">
                    <p>Use code <strong class="custom-primary">COFFEE2233 </strong> during checkout</p>
                    <h1 style="background-image: url('assets/img/cta/3.jpg') "> Coffee Robusta Roasted </h1>
                    <p>Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula.</p>
                    {{--<a href="shop-v1.html" class="andro_btn-custom">Shop Now</a>--}}
                </div>
            </div>
        </div>
        <div class="andro_banner-slider-inner" style="background-image: url('assets/img/banner/2.jpg');">
            <div class="container">
                <div class="andro_banner-slider-text">
                    <img src="assets/img/products/14.png" class="img-1" alt="product">
                    <p>Use code <strong class="custom-primary">COFFEE2233 </strong> during checkout</p>
                    <h1 style="background-image: url('assets/img/cta/2.jpg') "> Coffee Exotic Mocha </h1>
                    <p>Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula.</p>
                    {{--<a href="shop-v1.html" class="andro_btn-custom">Shop Now</a>--}}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Banner End -->

<!-- Categories Start -->
<div class="section section-padding category_section">
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-3 col-sm-4">
                <div class="andro_icon-block text-center has-link">
                    <a href="#">
                        <i class="flaticon-coffee-cup"></i>
                        <h5>Cafe Latte</h5>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-4">
                <div class="andro_icon-block text-center has-link">
                    <a href="#">
                        <i class="flaticon-coffee-beans"></i>
                        <h5>Mocha</h5>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-4">
                <div class="andro_icon-block text-center has-link">
                    <a href="#">
                        <i class="flaticon-coffee"></i>
                        <h5>Ice Coffee</h5>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-4">
                <div class="andro_icon-block text-center has-link">
                    <a href="#">
                        <i class="flaticon-coffee-1"></i>
                        <h5>Espresso</h5>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-4">
                <div class="andro_icon-block text-center has-link">
                    <a href="#">
                        <i class="flaticon-ice-coffee"></i>
                        <h5>Cappucino</h5>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-4">
                <div class="andro_icon-block text-center has-link">
                    <a href="#">
                        <i class="flaticon-moka-pot"></i>
                        <h5>Arabica</h5>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Categories End -->

<!-- CTA Start -->
<div class="section section-padding pt-0">
    <div class="container">
        <div class="row">

            <div class="col-md-6">
                <div class="andro_cta">
                    <img src="assets/img/cta/1.jpg" alt="cta">
                    <div class="andro_cta-content">
                        <h4 class="text-white">Freshly Picked <span class="fw-400">Robusta Roasted</span> </h4>
                        <p class="text-white">Cras ultricies ligula sed magna dictum porta. Proin eget tortor risus.</p>
                        {{--<a href="shop-v1.html" class="andro_btn-custom btn-sm light">Shop Now</a>--}}
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="andro_cta">
                    <img src="assets/img/cta/2.jpg" alt="cta">
                    <div class="andro_cta-content">
                        <h4 class="text-white">Freshly Picked <span class="fw-400">Mocha</span> </h4>
                        <p class="text-white">Cras ultricies ligula sed magna dictum porta. Proin eget tortor risus.</p>
                        {{--<a href="shop-v1.html" class="andro_btn-custom btn-sm light">Shop Now</a>--}}
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- CTA End -->

<!-- Icons Start -->
<div class="section section-padding pt-0">
    <div class="container">
        <div class="row">

            <div class="col-lg-4">
                <div class="andro_icon-block icon-block-2">
                    <i class="flaticon-coffee-cup"></i>
                    <h5>Guaranteed Coffee</h5>
                    <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="andro_icon-block icon-block-2">
                    <i class="flaticon-coffee-2"></i>
                    <h5>Daily Robustaing</h5>
                    <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="andro_icon-block icon-block-2">
                    <i class="flaticon-tag"></i>
                    <h5>Cheap & Creamyy</h5>
                    <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Icons End -->

<!-- Products Start -->
<menu-component :items="{{$products}}" />
<!-- Products End -->

<!-- Call to action Start -->
<div class="section pt-0">
    <div class="container">
        <div class="andro_cta-notice secondary-bg pattern-bg">
            <div class="andro_cta-notice-inner">
                <h3 class="text-white">Buy Today and Get 20% Off</h3>
                <p class="text-white">Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Cras ultricies ligula sed magna dictum porta. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi.</p>
            </div>
        </div>
    </div>
</div>
<!-- Call to action End -->

<!-- CTA Start -->
<div class="section section-padding pt-0">
    <div class="container">
        <div class="row">

            <div class="col-md-5">
                <div class="andro_cta">
                    <img src="assets/img/cta/3.jpg" alt="cta">
                    <div class="andro_cta-content">
                        <h4 class="text-white">Get Your <span class="fw-400">Coffee</span> </h4>
                        <p class="text-white">Cras ultricies ligula sed magna dictum porta. Proin eget tortor risus.</p>
                        {{--<a href="shop-v1.html" class="andro_btn-custom btn-sm light">Shop Now</a>--}}
                    </div>
                </div>
            </div>

            <div class="col-md-7">
                <div class="andro_cta">
                    <img src="assets/img/cta/4.jpg" alt="cta">
                    <div class="andro_cta-content">
                        <h4 class="text-white">Freshly Picked <span class="fw-400">Robusta Roasted</span> </h4>
                        {{--<a href="shop-v1.html" class="andro_btn-custom btn-sm light">Shop Now</a>--}}
                    </div>
                </div>
                <div class="andro_cta">
                    <img src="assets/img/cta/5.jpg" alt="cta">
                    <div class="andro_cta-content">
                        <h4 class="text-white">Freshly Picked <span class="fw-400">Mocha</span> </h4>
                        {{--<a href="shop-v1.html" class="andro_btn-custom btn-sm light">Shop Now</a>--}}
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- CTA End -->

@endsection
