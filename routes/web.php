<?php

    use App\Http\Controllers\HomeController;
    use App\Http\Controllers\OrderController;
    use App\Http\Controllers\ProductController;
    use App\Http\Controllers\ShippingController;
    use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);
Route::get('/checkout', [ShippingController::class, 'index']);
Route::post('/order', [OrderController::class, 'store'])->name('order');

Route::prefix('/cart')->group(function () {

    Route::get('/', [ProductController::class, 'cart']);
    Route::get('/add/{product}', [ProductController::class, 'addToCart'])->name('add-to-cart');
    Route::delete('/remove', [ProductController::class, 'removeFromCart'])->name('remove-from-cart');
    Route::patch('/update', [ProductController::class, 'updateCart'])->name('update-cart');

});



